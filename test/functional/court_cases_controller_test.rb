require 'test_helper'

class CourtCasesControllerTest < ActionController::TestCase
  setup do
    @court_case = court_cases(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:court_cases)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create court_case" do
    assert_difference('CourtCase.count') do
      post :create, court_case: @court_case.attributes
    end

    assert_redirected_to court_case_path(assigns(:court_case))
  end

  test "should show court_case" do
    get :show, id: @court_case.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @court_case.to_param
    assert_response :success
  end

  test "should update court_case" do
    put :update, id: @court_case.to_param, court_case: @court_case.attributes
    assert_redirected_to court_case_path(assigns(:court_case))
  end

  test "should destroy court_case" do
    assert_difference('CourtCase.count', -1) do
      delete :destroy, id: @court_case.to_param
    end

    assert_redirected_to court_cases_path
  end
end

class ApplicationController < ActionController::Base
	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :exception

	layout :layout_by_resource

	protected
		def after_sign_in_path_for(resource)
			flash[:notice] = "You have successfully signed in."
			stored_location_for(resource) || root_path
		end

		def after_sign_out_path_for(resource)
			flash[:notice] = "You have signed out"
			root_path
		end

		def layout_by_resource
			if devise_controller?
				'login'
			else
				'application'
			end
		end
end

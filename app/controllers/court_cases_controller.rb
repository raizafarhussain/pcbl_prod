class CourtCasesController < ApplicationController
  # GET /court_cases
  # GET /court_cases.json
  def index
    @court_cases = CourtCase.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @court_cases }
    end
  end

  # GET /court_cases/1
  # GET /court_cases/1.json
  def show
    @court_case = CourtCase.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @court_case }
    end
  end

  # GET /court_cases/new
  # GET /court_cases/new.json
  def new
    @court_case = CourtCase.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @court_case }
    end
  end

  # GET /court_cases/1/edit
  def edit
    @court_case = CourtCase.find(params[:id])
  end

  # POST /court_cases
  # POST /court_cases.json
  def create
    @court_case = CourtCase.new(params[:court_case])

    respond_to do |format|
      if @court_case.save
        format.html { redirect_to @court_case, notice: 'Court case was successfully created.' }
        format.json { render json: @court_case, status: :created, location: @court_case }
      else
        format.html { render action: "new" }
        format.json { render json: @court_case.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /court_cases/1
  # PUT /court_cases/1.json
  def update
    @court_case = CourtCase.find(params[:id])

    respond_to do |format|
      if @court_case.update_attributes(params[:court_case])
        format.html { redirect_to @court_case, notice: 'Court case was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @court_case.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /court_cases/1
  # DELETE /court_cases/1.json
  def destroy
    @court_case = CourtCase.find(params[:id])
    @court_case.destroy

    respond_to do |format|
      format.html { redirect_to court_cases_url }
      format.json { head :ok }
    end
  end
end

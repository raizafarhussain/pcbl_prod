// ================
// Case List Filter
// ================
function filterCaseList() {
	var date_from 		= $("#date_from").val();
	var date_to 		= $("#date_to").val();
	if( ((date_from == "") && (date_to != "")) || ((date_from != "") && (date_to == "")) ) {
		if (((date_from == "") && (date_to != ""))){
			alert("Date From field should not be empty.");
		} else if ((date_from != "") && (date_to == "")) {
			alert("Date To field should not be empty.");
		} else {
			alert("Please apply complete filter.");
		}
	} else {		
		var case_no		= encodeURIComponent($("#case_no").val());
		var case_title	= encodeURIComponent($("#case_title").val());
		var case_status	= encodeURIComponent($("#case_status").val());

		var url = "?";
		url = url + "case_no="+case_no;
		url = url + "&case_title="+case_title;
		url = url + "&case_status="+case_status;
		url = url + "&date_from="+date_from;
		url = url + "&date_to="+date_to;

		window.location = url;
	};
};

function resetCaseList() {
	$("#case_no").val("");
	$("#case_title").val("");
	$("#case_status").val("All");
	$("#date_from").val("");
	$("#date_to").val("");
	window.location.href = "court_cases";
};
// ================

// ===========================
// UnApproved Case List Filter
// ===========================
function filterUnApprovedCaseList() {
	var date_from 		= $("#date_from").val();
	var date_to 		= $("#date_to").val();
	if( ((date_from == "") && (date_to != "")) || ((date_from != "") && (date_to == "")) ) {
		if (((date_from == "") && (date_to != ""))){
			alert("Date From field should not be empty.");
		} else if ((date_from != "") && (date_to == "")) {
			alert("Date To field should not be empty.");
		} else {
			alert("Please apply complete filter.");
		}
	} else {		
		var case_no		= encodeURIComponent($("#case_no").val());
		var case_title	= encodeURIComponent($("#case_title").val());
		var case_status	= encodeURIComponent($("#case_status").val());

		var url = "?";
		url = url + "case_no="+case_no;
		url = url + "&case_title="+case_title;
		url = url + "&case_status="+case_status;
		url = url + "&date_from="+date_from;
		url = url + "&date_to="+date_to;

		window.location = url;
	};
};

function resetUnApprovedCaseList() {
	$("#case_no").val("");
	$("#case_title").val("");
	$("#case_status").val("All");
	$("#date_from").val("");
	$("#date_to").val("");
	window.location.href = "unapproved_cases";
};
// ===========================

// ===========================
// DisApprovedCase List Filter
// ===========================
function filterDisApprovedCaseList() {
	var date_from 		= $("#date_from").val();
	var date_to 		= $("#date_to").val();
	if( ((date_from == "") && (date_to != "")) || ((date_from != "") && (date_to == "")) ) {
		if (((date_from == "") && (date_to != ""))){
			alert("Date From field should not be empty.");
		} else if ((date_from != "") && (date_to == "")) {
			alert("Date To field should not be empty.");
		} else {
			alert("Please apply complete filter.");
		}
	} else {		
		var case_no		= encodeURIComponent($("#case_no").val());
		var case_title	= encodeURIComponent($("#case_title").val());
		var case_status	= encodeURIComponent($("#case_status").val());

		var url = "?";
		url = url + "case_no="+case_no;
		url = url + "&case_title="+case_title;
		url = url + "&case_status="+case_status;
		url = url + "&date_from="+date_from;
		url = url + "&date_to="+date_to;

		window.location = url;
	};
};

function resetDisApprovedCaseList() {
	$("#case_no").val("");
	$("#case_title").val("");
	$("#case_status").val("All");
	$("#date_from").val("");
	$("#date_to").val("");
	window.location.href = "disapproved_cases";
};
// ===========================

class CreateCourtCases < ActiveRecord::Migration
  def change
    create_table :court_cases do |t|
      t.string :case_no
      t.string :case_title
      t.string :legal_counsel
      t.integer :court
      t.text :description
      t.integer :case_status
      t.boolean :is_approved
      t.integer :user_id
      t.boolean :is_deleted

      t.timestamps
    end
  end
end
